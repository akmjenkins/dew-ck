<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Our Company
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity.
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p>
							Established in 2013, DEW Corp is a provincial leader in commercial and residential real estate, 
							as well as sports and entertainment. United by an entrepreneurial spirit, DEW Corp companies are 
							driven to take initiative, think creatively, and collaborate to achieve and use expertise to innovate. 
							Across each subsidiary, DEW Corp operates with a standard to expect more, developing unique 
							opportunities and communities where people want to live, work, and play.  Simply put, DEW Corp 
							believes in developing a better tomorrow for Newfoundland and Labrador.  
						</p>

						<p>
							As owner and operator of DEW Corp, Danny Williams has established himself as a business and community 
							leader with an incredibly diverse and impressive portfolio. His early success in establishing the largest 
							personal injury law firm in the province was followed by a very lucrative telecommunications career. 
							He then moved on to become the Province’s ninth Premier. Since then, Danny has re-entered the private 
							sector and continues to identify new opportunities.
						</p>

					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="sidebar-mod mod-links">
						<a href="#" class="selected">Our Company</a>
						<a href="#">Our Team</a>
						<a href="#">Contact Us</a>
					</div>
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>
	
	<section class="half-section grey-bg">
		<div class="lazybg" data-src="../assets/images/temp/split-block-1.jpg"></div>
		<div class="article-body">
			
			<h4>Mission</h4>
			
			<p>
				To provide an unparalleled standard of excellence through innovation, creativity, and superior customer service that will leave a legacy of hope and 
				compassion to those who need it most in our community.
			</p>
			
		</div><!-- .article-body -->
	</section><!-- .half-section -->
	
	<section class="half-section grey-bg">
		<div class="lazybg" data-src="../assets/images/temp/split-block-2.jpg"></div>
		<div class="article-body">
			
			<h4>Vision</h4>
			
			<p>
				Our goal is to create long-term prosperity and economic activity in Newfoundland and Labrador. We want to develop spaces where 
				people will yearn to live, work, and play by creating a real sense of place and belonging. We want to provide entertainment 
				opportunities for people to create memories and leave a life-long impression. And we wish to give back to the community in a 
				way that touches as many lives as possible in meaningful and tangible ways for generations to come.
			</p>
			
		</div><!-- .article-body -->
	</section><!-- .half-section -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>