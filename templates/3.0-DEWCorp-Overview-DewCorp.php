<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-3.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							DEW Corp
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="ov-blocks grid">
			
				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-1.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">						
								<div class="hgroup">
									<div class="hgroup-title">Our Company</div>
									<span class="hgroup-subtitle">A legacy of excellence.</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-2.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Our Team</div>
									<span class="hgroup-subtitle">Entrepreneurial focus.</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-3.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Careers</div>
									<span class="hgroup-subtitle">DEW Corp is always looking for new talent to join our team. Here is a list of our current employment opportunities.</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-4.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Contact Us</div>
									<span class="hgroup-subtitle">Building communities and boosting our economy.</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .ov-blocks -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>