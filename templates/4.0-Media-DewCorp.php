<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Media
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity.
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p>
							Pellentesque lacinia diam sed tristique varius. Mauris dictum ac purus ac cursus. Nullam imperdiet magna eu justo tempor sodales. 
							Fusce sapien nisi, varius id imperdiet vel, congue at sapien. Praesent commodo mauris sit amet lectus ornare placerat. Aliquam non 
							felis in nisl blandit iaculis nec dignissim felis. Aliquam erat volutpat. Nullam sed elit accumsan, blandit nibh id, rhoncus leo.
						</p>

						<p>
							Nam a posuere ex. In viverra maximus nibh iaculis eleifend. Integer ligula magna, laoreet ac malesuada a, auctor id felis. 
							Quisque malesuada libero at urna venenatis egestas. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur 
							ridiculus mus. Suspendisse aliquam non augue a semper. Nulla dictum efficitur orci, vitae pretium neque consequat eget.
						</p>

					</div><!-- .article-body -->
				</div><!-- .content -->
			</div><!-- .main-body -->		
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
			
			<div class="small-wrap center">
				<div class="hgroup center">
					<span class="hgroup-title">Brand Guidelines</span>
					<span class="hgroup-subtitle">Subtitle</span>
				</div><!-- .hgroup -->
				
				<p>
					Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, 
					in tempor eros arcu nec ipsum. In tempus mattis libero, sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam 
					ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet lectus magna. Nam et 
					rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
				</p>
				
				<a href="#" class="button darkblue">Download Guidelines</a>
			</div><!-- .small-wrap -->
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="grid">
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item center">
						<div class="mw-50">
							<img src="../assets/images/dew-corp.svg" alt="dew corp logo">
						</div><!-- .mw-90 -->
						<span class="block">Full Colour</span>
						<a href="#" class="button darkblue">Download</a>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item center">
						<div class="mw-50">
							<img src="../assets/images/dew-corp.svg" alt="dew corp logo">
						</div><!-- .mw-90 -->
						<span class="block">Full Colour</span>
						<a href="#" class="button darkblue">Download</a>
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2 xs-col-1">
					<div class="item center">
						<div class="mw-50">
							<img src="../assets/images/dew-corp.svg" alt="dew corp logo">
						</div><!-- .mw-90 -->
						<span class="block">Full Colour</span>
						<a href="#" class="button darkblue">Download</a>
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark-bg blue-bg">
		<div class="sw">
		
			<div class="hgroup center">
				<span class="hgroup-title">Media Contact</span>
				<span class="hgroup-subtitle">Subtitle</span>
			</div><!-- .hgroup -->
			
			<div class="small-wrap">
			
				<div class="grid collapse-750">
					<div class="col col-2">
						<div class="item">
							
							<strong>Media Contact Name</strong>
							<address>
								123 This Street <br />
								St. John's, NL A1B 2C3
							</address>
							
							<br />
							
							<span class="block">709.123.4567</span>
							<span class="block">709.123.4568</span>
							
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2">
					
						<div class="item">
						
							<form action="/" class="body-form">
								<div class="fieldset">
								
									<input type="text" name="name" placeholder="Name...">
									<input type="email" name="email" placeholder="Email...">
									<input type="tel" name="phone" placeholder="Phone...">
									<textarea name="message" placeholder="Message..."></textarea>
									
									<button class="button darkblue">Submit</button>
								
								</div><!-- .fieldset -->
							</form>
						
						</div><!-- .item -->
						
					</div><!-- .col -->
				</div><!-- .grid -->
			
			</div><!-- .small-wrap -->
			
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>