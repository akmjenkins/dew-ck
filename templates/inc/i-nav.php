<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw">
		
		<nav>
			<ul>
				<li><a class="active" href="#">Danny Williams Q.C.</a></li>
				<li><a class="active" href="#">DEW Corp</a></li>
				<li><a href="#">Real Estate</a></li>
				<li><a href="#">Entertainment</a></li>
				<li><a href="#">Golf</a></li>
				<li><a href="#">Philanthropy</a></li>
			</ul>
		</nav>
	
		<div class="nav-top">
			
			<div class="nav-top-links">
				<a href="#">Media</a>
				<a href="#">Contact</a>
			</div><!-- .nav-top-links -->
			
			<?php include('i-social.php'); ?>
			
			<form class="single-form global-search-form" action="/">
				<div class="fieldset">
					<input type="text" name="s" placeholder="Search">
					<button class="t-fa fa-search">&nbsp;</button>		
					<button type="button" class="t-fa fa-close toggle-search search-close">&nbsp;</button>
				</div><!-- .fieldset -->
			</form><!-- .single-form -->
			
		</div><!-- .nav-top -->
		
	</div><!-- .sw -->
	
</div><!-- .nav -->