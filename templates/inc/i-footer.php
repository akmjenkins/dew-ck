			<footer class="dark-bg">
				
				<div class="footer-nav">
					<ul>
						<li><a href="#">Danny Williams Q.C.</a></li>
						<li><a href="#">DEW Corp</a></li>
						<li><a href="#">Real Estate</a></li>
						<li><a href="#">Entertainment</a></li>
						<li><a href="#">Golf</a></li>
						<li><a href="#">Philanthropy</a></li>
					</ul>
				</div><!-- .footer-nav -->
				
				<div class="footer-contact">
				
					<address>
						P.O. Box 5236, 34 Harvey Road, 5th Floor <br />
						St. John's, NL A1C 5W1
					</address>
				
					<div class="flex">
						<span class="block">709.123.4567</span>
						<span class="block">709.123.4568</span>
					</div><!-- .flex -->
				
				</div><!-- .footer-contact -->
				
				<?php include('i-social.php'); ?>
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">DEW Corp</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .,sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/dew'
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>