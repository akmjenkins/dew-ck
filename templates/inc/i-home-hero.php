<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-1.jpg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<span class="title">Investing in the Future of Newfoundland &amp; Labrador</span>
					<span class="subtitle">
						Delivering Growth and Opportunity
					</span><!-- .subtitle -->
					
					<p>
						Committed to the long-term prosperity of Newfoundland & Labrador, DEW Corporation is parent to a diverse  portfolio of local businesses and real estate developments. 
					</p>
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->