<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Our Team
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity.
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p>
							Established in 2013, DEW Corp is a provincial leader in commercial and residential real estate, as well as sports and entertainment. 
							United by an entrepreneurial spirit, DEW Corp companies are driven to take initiative, think creatively, and collaborate to achieve and 
							use expertise to innovate. Across each subsidiary, DEW Corp operates with a standard to expect more, developing unique opportunities 
							and communities where people want to live, work, and play.  Simply put, DEW Corp believes in developing a better tomorrow for 
							Newfoundland and Labrador.  
						</p>

						<p>
							As owner and operator of DEW Corp, Danny Williams has established himself as a business and community leader with an incredibly 
							diverse and impressive portfolio. His early success in establishing the largest personal injury law firm in the province was followed 
							by a very lucrative telecommunications career. He then moved on to become the Province’s ninth Premier. Since then, Danny has 
							re-entered the private sector and continues to identify new opportunities.
						</p>

					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="sidebar-mod mod-links">
						<a href="#">Our Company</a>
						<a href="#" class="selected">Our Team</a>
						<a href="#">Contact Us</a>
					</div>
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<div class="grid">
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-1.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-2.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-3.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-4.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-5.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-3 sm-col-2">
					<div class="item center">
					
						<a href="#" class="tm-wrap bounce sm">
							<div class="tm lazybg img" data-src="../assets/images/temp/tm-6.jpg">&nbsp;</div>
						</a>
						
						<span class="tm-name">Team Member</span>
						<small class="tm-title">Job Title</small>
						
						<a href="#" class="darkblue button">View</a>
					
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grdi -->
		
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>