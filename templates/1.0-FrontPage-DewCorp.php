<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<!--

	New change to lazy loading images on elementswith a data-src attribute.
	
	You can now load multiple images, depending on resolution. Right now, they are only width constrained, meaning you can only change the image based on the browsers width.
	
	Example:
		data-src="
			http://dummyimage.com/1920x500/000/fff, //default image (loaded if none of the conditions to load other images are met)
			http://dummyimage.com/1200x500/000/fff 1200w, //image loaded when the browser is less than or equal to 1200px wide
			http://dummyimage.com/600x500/000/fff 600w, //image loaded when the browser is less than or equal to 600px wide
		"

-->

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/images/temp/hero/hero-1.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<span class="title">Investing in the Future of Newfoundland &amp; Labrador</span>
					<span class="subtitle">
						Delivering Growth and Opportunity
					</span><!-- .subtitle -->
					
					<p>
						Committed to the long-term prosperity of Newfoundland & Labrador, DEW Corporation is parent to a diverse  portfolio of local businesses and real estate developments. 
					</p>
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="ov-blocks grid">
			
				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-1.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">						
								<div class="hgroup">
									<div class="hgroup-title">Real Estate</div>
									<span class="hgroup-subtitle">Building Communities and boosting our economy</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-2.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Entertainment</div>
									<span class="hgroup-subtitle">Catch the greatest sports and events.</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-3.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Golf</div>
									<span class="hgroup-subtitle">Everything you need for a relaxing day on the green</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item ov-item bounce dark-bg" href="#">
						<div class="ov-bgimage">
							<div class="lazybg img"  data-src="../assets/images/temp/block-4.jpg"></div>
						</div><!-- .ov-bgimage -->
						<div class="ov-content-wrap">
							<div class="ov-content">
								<div class="hgroup">
									<div class="hgroup-title">Philanthropy</div>
									<span class="hgroup-subtitle">A legacy of hope and compassion</span>
								</div><!-- .hgroup -->
								
								<span class="button blue">Read More</span>
							</div><!-- .ov-content -->
						</div><!-- .ov-content-wrap -->
					</a><!-- .item -->
				</div><!-- .col -->

				
			</div><!-- .ov-blocks -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>