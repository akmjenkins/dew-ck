<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Contact Us
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity.
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">

						<p>
							Established in 2013, DEW Corp is a provincial leader in commercial and residential real estate, as well as sports and entertainment. 
							United by an entrepreneurial spirit, DEW Corp companies are driven to take initiative, think creatively, and collaborate to achieve 
							and use expertise to innovate. Across each subsidiary, DEW Corp operates with a standard to expect more, developing unique opportunities 
							and communities where people want to live, work, and play.  Simply put, DEW Corp believes in developing a better tomorrow for 
							Newfoundland and Labrador.  
						</p>

						<p>
							As owner and operator of DEW Corp, Danny Williams has established himself as a business and community leader with an incredibly 
							diverse and impressive portfolio. His early success in establishing the largest personal injury law firm in the province was followed by 
							a very lucrative telecommunications career. He then moved on to become the Province’s ninth Premier. Since then, Danny has 
							re-entered the private sector and continues to identify new opportunities.
						</p>

					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="sidebar-mod mod-links">
						<a href="#">Our Company</a>
						<a href="#">Our Team</a>
						<a href="#" class="selected">Contact Us</a>
					</div>
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>

	<section class="grey-bg">
		<div class="sw">
		
			<div class="grid eqh collapse-1000">
				<div class="col-2 col">
					<div class="item">
					
						<div class="contact-wrap">
							
							<div class="contact-address">
							
								<strong>Address</strong>
								<address>
									123 This Street <br />
									St. John's, NL A1B 2C3
								</address>
								
								<br />
								
								<span class="block">709.123.4567</span>
								<span class="block">709.123.4568</span>
								
							</div><!-- .contact-address -->
							
							<div class="contact-form">
							
								<form action="/" class="body-form">
									<div class="fieldset">
									
										<input type="text" name="name" placeholder="Name...">
										<input type="email" name="email" placeholder="Email...">
										<input type="tel" name="phone" placeholder="Phone...">
										<textarea name="message" placeholder="Message..."></textarea>
										
										<button class="button darkblue">Submit</button>
									
									</div><!-- .fieldset -->
								</form><!--.body-form -->
							
							</div><!-- .contact-form -->
							
						</div><!-- .contact-wrap -->
			
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2">
					<div class="item embedded-gmap-wrap">
					
						<div class="embedded-gmap">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1346.074908850923!2d-52.71411039999999!3d47.56487340000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca3bcf6218969%3A0x30b59d2714d7bd39!2s82+Harvey+Rd%2C+St.+John&#39;s%2C+NL+A1C+2G1!5e0!3m2!1sen!2sca!4v1423853227467" frameborder="0" style="border:0"></iframe>
						</div><!-- .embedded-gmap -->
					
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grdi -->
		
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>