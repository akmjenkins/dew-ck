<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Williams Family Foundation
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							A leader in provincial prosperity.
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p>
							Phasellus quis finibus augue, nec venenatis metus. Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper. 
							Aliquam non elementum elit, nec ultricies turpis. Ut cursus tempus augue. Morbi consectetur justo sit amet est dictum, quis consectetur 
							nunc ornare. Proin cursus lacinia aliquam. Donec rutrum sodales mattis. Nunc quis fringilla mauris, at interdum augue. 
							Phasellus sed aliquam lectus, ut rutrum quam. Aenean congue magna et sapien venenatis, at laoreet nisl porta. Nam porta vestibulum 
							pellentesque. Donec id tristique massa, at lacinia lectus.
						</p>
						
						<h2>Header 2 - H2</h2>
						<h3>Header 3 - H3</h3>
						<h4>Header 4 - H4</h4>
						<h5>Header 5 - H5</h5>
						<h6>Header 6 - H6</h6>

						<p>
							Nullam cursus, dui eget imperdiet dapibus, leo dui pretium libero, non facilisis massa felis et lacus. Suspendisse rutrum euismod turpis 
							vitae commodo. Sed in ante vel felis rutrum iaculis eget vitae ipsum. Praesent sollicitudin eros eu orci elementum porttitor. Aliquam efficitur 
							imperdiet volutpat. Pellentesque eget vestibulum dolor. Nunc sit amet pulvinar justo.
						</p>
						
						<h3>Links</h3>
						<p>
						
							<em>
								Note: styles apply to links in paragraphs/lists/blockquotes, or <code>&lt;a&gt;</code> tags with a class of <code>.inline</code>
							</em>
							
							<br />
							<br />
						
							<a href="#link">Link</a>
							<a href="#link" class="hover">Link:hover</a>
							<a href="#link" class="visited">Link:visited</a>
							<a href="#link" class="active">Link:active</a>
							
							<br />
							<br />
							
							And this is what a link will <a href="#link">look like</a> when it is written in a <a href="#link">regular paragraph</a>.
							
						</p>
						
						<hr />
						
						<ul>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ul>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ul>
							</li>
						</ul>
						
						<ol>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ol>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ol>
							</li>
						</ol>

					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="sidebar-mod mod-links">
						<a href="#" class="selected">Williams Family Foundatin</a>
						<a href="#">IceCaps Cares</a>
					</div>
					
					<div class="sidebar-mod">
						<div class="bordered pad-20 center">
							<a href="#">
								<img src="../assets/images/williams-foundation.png" alt="Williams Foundation Logo">
							</a>
						</div><!-- .bordered -->
						<a href="#" class="button block blue square">Visit Website</a>
					</div><!-- .sidebar-mod -->
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>