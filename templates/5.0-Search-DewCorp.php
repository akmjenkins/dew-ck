<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="../assets/images/temp/hero/hero-2.jpg">
		
			<div class="hero-caption dark-bg">
				<div class="sw">
					
					<div class="hgroup">
						<span class="hgroup-title">
							Search results for "Query"
						</span><!-- .hgroup-title -->
						<span class="hgroup-subtitle">
							Subtitle
						</span><!-- .hgroup-subtitle -->
					</div><!-- .hgroup -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">

			<div class="filter-section">
				<div class="sw">
				
					<div class="filter-bar">

						<div class="filter-bar-left">
						
							<div class="count">
								10 Results
							</div><!-- .count -->
							
						</div><!-- .filter-bar-left -->

						<div class="filter-bar-meta">
							
							<div class="filter-controls">
								<button class="previous">Prev</button>
								<button class="next">Next</button>
							</div><!-- .filter-controls -->
						
						</div><!-- .filter-bar-meta -->
							
					</div><!-- .filter-bar -->
					
				</div><!-- .sw -->
				
				<div class="filter-content">
				
					<div class="sw">
					
						<div class="grid eqh search-grid">
							<div class="col">
								<div class="item">
								
									<div class="hgroup">
										<span class="hgroup-title">Page Title</span>
										<span class="hgroup-subtitle">Subtitle</span>
									</div><!-- .hgroup -->
									
									<p>
										Phasellus congue est nunc. Maecenas vitae ipsum dui. In hendrerit tellus nec sapien imperdiet aliquet non non nibh. Cras aliquet lacinia ante, sit amet facilisis orci iaculis ornare. Nunc vitae vehicula erat.
									</p>
								
									<a href="#" class="button blue">More</a>
								
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col">
								<div class="item">
								
									<div class="hgroup">
										<span class="hgroup-title">A much much longer Page Title</span>
										<span class="hgroup-subtitle">Subtitle</span>
									</div><!-- .hgroup -->
									
									<p>
										Phasellus congue est nunc. Maecenas vitae ipsum dui. In hendrerit tellus nec sapien imperdiet aliquet non non nibh. 
									</p>
								
									<a href="#" class="button blue">More</a>
								
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col">
								<div class="item">
								
									<div class="hgroup">
										<span class="hgroup-title">Page Title</span>
										<span class="hgroup-subtitle">Subtitle</span>
									</div><!-- .hgroup -->
									
									<p>
										Phasellus congue est nunc. Maecenas vitae ipsum dui. In hendrerit tellus nec sapien imperdiet aliquet non non nibh. Cras aliquet lacinia ante, sit amet facilisis orci iaculis ornare. Nunc vitae vehicula erat.
									</p>
								
									<a href="#" class="button blue">More</a>
								
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col">
								<div class="item">
								
									<div class="hgroup">
										<span class="hgroup-title">A much much longer Page Title</span>
										<span class="hgroup-subtitle">Subtitle</span>
									</div><!-- .hgroup -->
									
									<p>
										Phasellus congue est nunc. Maecenas vitae ipsum dui. In hendrerit tellus nec sapien imperdiet aliquet non non nibh. 
									</p>
								
									<a href="#" class="button blue">More</a>
								
								</div><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
					
					</div><!-- .sw -->
					
				</div><!-- .filter-content -->
			</div><!-- .filter-section -->

		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>