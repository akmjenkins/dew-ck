<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<!-- 
	any hero with content overlaid on the image (other than a caption)
	needs a class of "with-content" to prevent the hero from collapsing on mobile
-->
<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item">		
			<div class="fader-item-bg" data-src="
				../assets/images/temp/hero/about-hero.jpg,
				http://dummyimage.com/1200x500/000/fff 1200w,
				http://dummyimage.com/600x500/000/fff 600w,
			"></div>
			<div class="hero-content-wrap sw">
				<div class="hero-content">
					<h1 class="title">Danny Williams Q.C. LLB, Hon. DD.L</h1>
					
					<p>
						Danny Williams has quickly become a household name in Newfoundland and Labrador, and all across Canada. As a business and community leader, 
						he has established a diverse portfolio that will leave a lasting legacy. And today, as owner and operator of DEW Corp. Mr. Williams continues to create 
						long-term prosperity and growth for the province he proudly calls home.
					</p>
				</div><!-- .hero-content -->
			</div><!-- .hero-content-wrap -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p>
							Mr. Williams studied political science and economics at Memorial University of Newfoundland. Awarded the Rhodes Scholarship in 1969, he received a Bachelor of 
							Arts in Law from England’s Oxford University before returning to Canada to earn a Bachelor of Law degree from Dalhousie University. In 1972, he began 
							practicing law and was appointed to the Queen’s Counsel in 1984. 
						</p>
						
						<p>
							While studying at Dalhousie, Mr. Williams led a consortium of business people seeking Newfoundland and Labrador’s first cable television license. In 1975, he 
							guided Cable Atlantic through acquisitions of systems across the province to become one of the largest telecommunications companies in Atlantic Canada. 
						</p>
							
						<p>
							Entering public life, Mr. Williams was elected as the Member for Humber West and served as Newfoundland and Labrador Progressive Conservative Party 
							Leader from 2001 to 2010. He went on to win the government in 2003 when he became the 9th Premier of Newfoundland and Labrador. In 2010, Mr. Williams 
							announced his retirement from political life; at the time his government’s approval ratings were above 80 per cent. 
						</p>
							
						<p>
							Mr. Williams has served as a Chair Member of the Canadian Parliamentary Channel, the Newfoundland and Labrador Film Development Corporation, and 
							the Provincial Government Offshore Oil Impact Advisory Council. He is also a member of many charitable organizations, such as the Terry Fox Marathon of 
							Hope, the Vera Perlin Society, and Big Brothers/Big Sisters. He is a member of the Newfoundland and Labrador Business Hall of Fame and has been the recipient 
							of many business, sport, and voluntary awards and distinctions.
						</p>



					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar">
					
					<div class="sidebar-mod mod-images">
						
						<div class="lazybg">
							<img src="../assets/images/temp/side-1.jpg" alt="o danny boy">
						</div><!-- .lazybg -->
						
						<div class="lazybg">
							<img src="../assets/images/temp/side-2.jpg" alt="the pipes the pipes...">
						</div><!-- .lazybg -->
						
					</div><!-- .mod-images -->
					
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>