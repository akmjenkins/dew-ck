var ns = 'JAC-DEW';
window[ns] = {};

// @codekit-append "scripts/debounce.js"
// @codekit-append "scripts/tests.js"

// @codekit-append "scripts/anchors.external.popup.js"
// @codekit-append "scripts/standard.accordion.js"
// @codekit-append "scripts/custom.select.js"
// @codekit-append "scripts/aspect.ratio.js"
// @codekit-append "scripts/image.loader.js"
// @codekit-append "scripts/lazy.images.js"
// @codekit-append "scripts/tabs.js"
// @codekit-append "scripts/nav.js"
// @codekit-append "scripts/swiper.js"
// @codekit-append "scripts/magnific.popup.js"
// @codekit-append "scripts/preventOverScroll.js"

// @codekit-append "global.js"