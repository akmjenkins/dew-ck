;(function(context) {

	var $window = $(window);

	var ImageLoader = (function() {
		
		//cache
		var loaded = [];
		
		return {
			
			/**
			 *	data-src on images can either be a single string (path to an image), or they can resemble the srcset specification
			 *
			 *		data-src="
			 *			/path/to/image.jpg, //default image 
			 *			/path/to/image.jpg 800w, //image loaded when screen is equal to and below 800px wide
			 *			/path/to/image.jpg 320w, //image loaded when screen is equal to and below 320px wide
			 *		"
			**/
			getAppropriateSource: function(sourceString) {
				var source;
				var sourceSizes = [];
				var sourceObject = {};
				var sources = sourceString.split(',');
				if(sources.length === 1) {
					return sources[0];
				}
				

				$.each(sources,function(i,source) {
					var s = $.trim(source);
					if(!s.length) { return; }
					var sourceItem = s.split(' ');
					var source = sourceItem[0];
					var size = parseInt(sourceItem[1],10);
					size = isNaN(size) ? Number.MAX_SAFE_INTEGER : size;
					
					sourceSizes.push(size);
					sourceObject[size] = source;
				});
				
				
				//sort the sizes numerically asc
				sourceSizes.sort(function(a,b) { return a-b; });
				
				$.each(sourceSizes,function(i,size) {
					if($window.innerWidth() <= size) {
						source = sourceObject[size];
						return false;
					}
				});
				
				return source;
			},
			
			hasSourceLoaded: function(source) {
				return $.inArray(source,loaded) !== -1;
			},
			
			loadSource: function(source) {
				var self = this;
				var dfd = $.Deferred();
				
				$('<img/>')
					.on('load',function() {
						self.hasSourceLoaded() || loaded.push(source);
						dfd.resolve();
					})
					.on('error',function() {
						dfd.reject();
					})
					.attr('src',source);
					
				return dfd;
			}
			
		}
		
	}());

	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = ImageLoader;
	//CodeKit
	} else if(context) {
		context.imageLoader = ImageLoader;
	}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));