;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		hideDropdownTimeout,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$html = $('html'),
		$body = $('body'),
		$searchForm = $('.global-search-form'),
		$searchInput = $('input',$searchForm),
		startingNavOffset = $('.nav').height(),
		SHOW_SEARCH_CLASS = 'show-search',
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav',
		SHOW_DROPDOWN_CLASS = 'show-dropdown',
		COLLAPSE_NAV_AT = 875;

	var methods = {
	
		isShortScreen: function() {
			return $window[0].innerHeight < SMALL_DROPDOWNS_AT_HEIGHT;
		},
	
		checkShowSmallNav: function() {
			if(!this.isNavCollapsed() && $window.scrollTop() > startingNavOffset) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},
		
		isNavCollapsed: function() {
			return $window[0].innerWidth < COLLAPSE_NAV_AT;
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
			this.isNavCollapsed() || this.showNav(false);
		},
	
		showNav: function(show) {
			$html[show ? 'addClass' : 'removeClass'](SHOW_CLASS);
		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		},
		
		showSearch: function(show) {
			var $input = $('input',$searchForm);
			
			if(show) {
				$html.addClass(SHOW_SEARCH_CLASS);
				$input.focus();
			} else {
				$html.removeClass(SHOW_SEARCH_CLASS);
				$input.blur();
			}
		},
		
		toggleSearch: function() {
			this.showSearch(!this.isShowingSearch());
		},
		
		isShowingSearch: function() {
			return $html.hasClass(SHOW_SEARCH_CLASS);
		}

	};
	
	$searchForm
		.on('submit',function() {
			if(!$.trim($searchInput.val()).length) {
				methods.toggleSearch();
				return false;	
			}
		});
	
	//listeners
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				if(methods.isShowingSearch()) {
					methods.showSearch(false);
					return false;
				}
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		})
		.on('click','.toggle-search',function() {
			methods.toggleSearch();
		})
		.on('keypress',function(e) {
			if(e.keyCode === 63 || (e.shiftKey && e.keyCode === 191)) {
				methods.showSearch(true);
			}
		})
		.on('click','nav div',function(e) {
			if(e.currentTarget.nodeName.toLowerCase() === 'div') {
				$(e.currentTarget).parent().toggleClass('expanded');
			}
		})

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(tests.ios()) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));